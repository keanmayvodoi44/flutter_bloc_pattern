class Users{
  String account;
  String password;

  Users({this.account, this.password});

  factory Users.fromJson(Map<String, dynamic> json){
    return Users(
      account: json['account'],
      password: json['password']
    );
  }
}