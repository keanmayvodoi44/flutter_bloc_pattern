import 'package:demo_flutter_bloc/src/models/images.dart';
import 'package:demo_flutter_bloc/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:demo_flutter_bloc/src/blocs/home_bloc.dart';

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

class HomeState extends State<Home>{

  final GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc.fetchImages(_scaffoldState, context);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldState,
      appBar: _buildAppBar(),
      body: _buildGridView(),
    );
  }

  Widget _buildAppBar(){
    return AppBar(
      title: Text("Trang chính"),
      centerTitle: true,
    );
  }

  Widget _buildProgressDialog(){
    return Opacity(
      opacity: 0.5,
      child: Container(
        color: Colors.grey,
        height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildGridView(){
    return StreamBuilder(
      stream: bloc.outputLoadImages,
      builder: (context, snapshot){
        if(!snapshot.hasData)
          return _buildProgressDialog();
        else if(snapshot.hasError){
          return Container();
        }
        else if(snapshot.hasData){
          return GridView.count(
            crossAxisCount: 2,
            children: _buildGridTile(snapshot.data),
          );
        }
      },
    );
  }

  List<Widget> _buildGridTile(List<Images> images){
    List<Container> containers = List<Container>.generate(images.length, (index){
    return Container(
    padding: EdgeInsets.all(5.0),
    child: FadeInImage.assetNetwork(
    image: BASE_URL + images[index].url,
    placeholder: 'assets/loading.gif',
    fit: BoxFit.fill,
    )
    );
    });
    return containers;
  }
}