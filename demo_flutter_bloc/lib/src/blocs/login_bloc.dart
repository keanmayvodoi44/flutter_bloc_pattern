import 'package:demo_flutter_bloc/src/ui/home.dart';
import 'package:demo_flutter_bloc/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'package:demo_flutter_bloc/src/resources/repository.dart';

class LoginBloc{
  final _repository = Repository();
  final _isLogin = PublishSubject<bool>();
  final _isEnableButton = PublishSubject<bool>();
  final _accountText = PublishSubject<String>();
  final _passwordText = PublishSubject<String>();

  bool _accountIsEmpty = true;
  bool _passwordIsEmpty = true;

  Observable<bool> get outputIsLogin => _isLogin.stream;
  Observable<bool> get outputIsEnableButton => _isEnableButton.stream;
  Sink get inputAccountText => _accountText;
  Sink get inputPasswordText => _passwordText;

  LoginBloc(){
    this._accountText.stream.listen((data) {
      if(data != ""){
        _accountIsEmpty = false;
      }
      else{
        _accountIsEmpty = true;
      }
      checkEmpty();
    });

    this._passwordText.stream.listen((data){
      if(data != ""){
        _passwordIsEmpty = false;
      }
      else{
        _passwordIsEmpty = true;
      }
      checkEmpty();
    });
  }

  void checkEmpty(){
    if(_accountIsEmpty == false && _passwordIsEmpty == false){
      _isEnableButton.add(true);
    }
    else{
      _isEnableButton.add(false);
    }
  }

  void login(GlobalKey<ScaffoldState> scaffoldState, BuildContext context, String account, String password) async {
    _isLogin.sink.add(true);
    _repository.fetchUser(account, password).then((data){
      _isLogin.sink.add(false);
      if(data == 1){
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => Home()
            )
        );
      }
      else{
        showSnackbar(scaffoldState, "Tài khoản không hợp lệ");
      }
    }).catchError((onError){
      _isLogin.sink.add(false);
      showSnackbar(scaffoldState, "Không thể kết nối đến Server");
    });

  }

  void dispose(){
    _accountText.close();
    _passwordText.close();
    _isLogin.close();
    _isEnableButton.close();
  }
}

final bloc = LoginBloc();