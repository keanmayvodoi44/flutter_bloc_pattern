import 'package:demo_flutter_bloc/src/models/images.dart';
import 'package:demo_flutter_bloc/src/resources/repository.dart';
import 'package:demo_flutter_bloc/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final _repository = Repository();
  final _listImages = PublishSubject<List<Images>>();

  Observable<List<Images>> get outputLoadImages => _listImages.stream;

  void fetchImages(GlobalKey<ScaffoldState> scaffoldState, BuildContext context) async {
    await _repository.fetchAllImages().then((data){
      _listImages.sink.add(data);
    }).catchError((onError){
      showSnackbar(scaffoldState, "Không thể kết nối đến Server");
    });
  }

  void dispose() {
    // TODO: implement dispose
    _listImages.close();
  }
}

final bloc = HomeBloc();