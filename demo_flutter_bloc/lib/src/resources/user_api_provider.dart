import 'package:http/http.dart' as http;

import 'package:demo_flutter_bloc/utils/utility.dart';

class UserApiProvider{
  Future<int> login(String account, String password) async {
    http.Response response = await http.post(
        BASE_URL + LOGIN_URL,
        body: {
          'account': account,
          'password': password
        }
    );

    if(response.statusCode == 200){
      if(response.body == 'failt'){
        return -1;
      }
      else{
        return 1;
      }
    }
    else{
      throw Exception("Error while fetching data");
    }
  }
}