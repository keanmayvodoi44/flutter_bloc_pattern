import 'dart:convert';

import 'package:demo_flutter_bloc/src/models/images.dart';
import 'package:http/http.dart' as http;

import 'package:demo_flutter_bloc/utils/utility.dart';

class ImageApiProvider{
  Future<List<Images>> fetch() async {
    http.Response response = await http.get(
      BASE_URL + GETIMAGES_URL,
    );

    if(response.statusCode == 200){
      final mapResponse = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Images> listImages = await mapResponse.map<Images>((json){
        return Images.fromJson(json);
      }).toList();
      return listImages;
    }
    else{
      throw Exception("Error while fetching data");
    }
  }
}