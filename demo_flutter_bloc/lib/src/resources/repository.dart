import 'package:demo_flutter_bloc/src/models/images.dart';
import 'package:demo_flutter_bloc/src/resources/image_api_provider.dart';
import 'package:demo_flutter_bloc/src/resources/user_api_provider.dart';

class Repository{
  final _userApiProvider = UserApiProvider();
  final _imageApiProvider = ImageApiProvider();

  Future<int> fetchUser(String account, String password) => _userApiProvider.login(account, password);

  Future<List<Images>> fetchAllImages() => _imageApiProvider.fetch();
}